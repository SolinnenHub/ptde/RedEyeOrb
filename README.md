# RedEyeOrb

RedEyeOrb is an implementation of Dark Souls 3 [Red Eye Orb](https://darksouls.fandom.com/wiki/Red_Eye_Orb_(Dark_Souls_III)).

This plugin requires an updated PTDE resource pack to work:

https://gitlab.com/SolinnenHub/ptde/ptde-resource-pack


## Mechanics

The item only works in the Nether.

Right-clicking causes the Nether Eye to move in the direction of the nearest player.

### Commands:

`/orb` - get the orb.

[![ RedEyeOrb demo ](preview.jpg)](https://youtu.be/XprcWtyy0Kc " RedEyeOrb demo ")

## How to build:

```bash
./build.sh
```