package net.solinnen.RedEyeOrb;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EnderSignal;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static org.bukkit.util.NumberConversions.round;

public class RedEyeOrb extends JavaPlugin implements Listener, CommandExecutor {

    private final int COOLDOWN_TICKS = 80;
    private final int COOLDOWN_MS = COOLDOWN_TICKS*50;
    private final int CUSTOM_MODEL_DATA = 7;

    private final HashMap<UUID, Long> orbUsageTimestamps = new HashMap<>(50);
    private final HashMap<UUID, UUID> targets = new HashMap<>(100);

    @Override
    public void onEnable() {
        Objects.requireNonNull(this.getCommand("orb")).setExecutor(this);

        PluginManager plugman = getServer().getPluginManager();
        plugman.registerEvents(this, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            ItemStack orb = new ItemStack(Material.ENDER_EYE);
            ItemMeta meta = orb.getItemMeta();
            assert meta != null;
            meta.setDisplayName(ChatColor.DARK_PURPLE+"Око Нижнего мира");
            meta.setLore(new ArrayList<>(Collections.singletonList(ChatColor.GRAY+"Указывает путь к сильной душе")));
            meta.setCustomModelData(CUSTOM_MODEL_DATA);
            orb.setItemMeta(meta);
            player.getInventory().addItem(orb);
            sender.sendMessage(ChatColor.GOLD+"[RedEyeOrb] "+ChatColor.RESET+"Получено Око Нижнего мира.");
        } else {
            sender.sendMessage(ChatColor.RED + "Эту команду можно выполнять только из игры.");
        }
        return true;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    private void onPlayerInteractEvent(PlayerInteractEvent event) {
        if (event.getHand() == null) {
            return;
        }
        if (event.getHand().equals(EquipmentSlot.HAND) || event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                ItemStack itemInHand = event.getItem();
                if (itemInHand == null) {
                    return;
                }
                ItemMeta meta = itemInHand.getItemMeta();
                if (meta == null || !meta.hasCustomModelData()) {
                    return;
                }
                if (itemInHand.getType().equals(Material.ENDER_EYE) && meta.getCustomModelData() == CUSTOM_MODEL_DATA) {
                    Player player = event.getPlayer();
                    if (player.getWorld().getEnvironment().equals(World.Environment.NETHER)) {
                        UUID uuid = player.getUniqueId();
                        long currentTime = System.currentTimeMillis();
                        if (currentTime - orbUsageTimestamps.getOrDefault(uuid, 0L) > COOLDOWN_MS) {
                            orbUsageTimestamps.put(uuid, currentTime);
                            player.setCooldown(Material.ENDER_EYE, COOLDOWN_TICKS);
                            player.getWorld().playSound(player.getLocation(), "custom.red_eye_orb", SoundCategory.AMBIENT, 0.6f, 1);
                            EnderSignal eye = player.getWorld().spawn(player.getEyeLocation(), EnderSignal.class);
                            eye.setItem(new ItemStack(itemInHand));
                            eye.setDespawnTimer(60);

                            ArrayList<Player> anotherPlayersInNether = new ArrayList<>();
                            for (Player _player : this.getServer().getOnlinePlayers()) {
                                if (_player.getWorld().getEnvironment().equals(World.Environment.NETHER) &&
                                        !_player.getUniqueId().equals(player.getUniqueId())) {
                                    anotherPlayersInNether.add(_player);
                                }
                            }

                            if (anotherPlayersInNether.size() == 0) {
                                Location loc = player.getLocation();
                                eye.setTargetLocation(new Location(player.getWorld(), loc.getX(), loc.getY()+3, loc.getZ()));
                            } else {
                                Random rand = new Random();
                                UUID currentTargetUuid = targets.get(uuid);
                                Player anotherPlayerInNether;
                                if (currentTargetUuid == null) {
                                    anotherPlayerInNether = anotherPlayersInNether.get(rand.nextInt(anotherPlayersInNether.size()));
                                } else {
                                    Player anotherPlayerByUuid = this.getServer().getPlayer(currentTargetUuid);
                                    if (anotherPlayerByUuid == null) {
                                        anotherPlayerInNether = anotherPlayersInNether.get(0);
                                    } else {
                                        if (!anotherPlayerByUuid.getWorld().getEnvironment().equals(World.Environment.NETHER)) {
                                            anotherPlayerInNether = anotherPlayersInNether.get(rand.nextInt(anotherPlayersInNether.size()));
                                        } else {
                                            anotherPlayerInNether = anotherPlayerByUuid;
                                        }
                                    }
                                }
                                targets.put(uuid, anotherPlayerInNether.getUniqueId());
                                Location loc = anotherPlayerInNether.getLocation();
                                double distance = loc.distance(player.getLocation());
                                int radius = round(distance * 0.15);
                                eye.setTargetLocation(new Location(player.getWorld(),
                                        loc.getX()+rand.nextInt(2*radius+1) - radius,
                                        loc.getY()+3,
                                        loc.getZ()+rand.nextInt(2*radius+1) - radius));
                            }

                            eye.setDropItem(false);

                            if (player.getGameMode() != GameMode.CREATIVE) {
                                itemInHand.setAmount(itemInHand.getAmount() - 1);
                            }
                        }
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    private void onPlayerQuitEvent(PlayerQuitEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        orbUsageTimestamps.remove(uuid);
        targets.remove(uuid);
    }

    @Override
    public void onDisable() {}

}
